import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from 'react-apollo';

import UserList from './components/userList'

//Creating Apollo Client for graphQL endpoint
const client = new ApolloClient({
  link: new HttpLink({ uri: "http://localhost:3000/graphql" }),
  cache: new InMemoryCache(),
});

export default class App extends React.Component<{}> {
  render() {
    return (
      <ApolloProvider client={client}>
        <UserList/>
      </ApolloProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
