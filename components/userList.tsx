import * as React from 'react'
import {
    FlatList,
    View,
    ActivityIndicator,
    StyleSheet,
} from 'react-native'
import UserRow from './userRow'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'

class UserList extends React.Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        const userList = this.props;
    }
    renderList(userList){
        return(
            <View style={{flex:1,marginTop:44}}>
                <FlatList 
                    data={userList}
                    renderItem={({item})=>(
                        <UserRow data={item}/>
                    )}
                    keyExtractor={item => item._id}
                    contentContainerStyle={{borderBottomWidth:0}}
                />
            </View>
        )   
    }

    renderLoading(){
        return(  <View style={styles.container}>
            <ActivityIndicator styleAttr='Large'/>
          </View>)
      }
      renderError(){
        return(  <View style={styles.container}>
                    <Text style={styles.welcome}>
                      An error occured
                    </Text>
          </View>)
      }

    render(){
        if(this.props.allUsersQuery && this.props.allUsersQuery.loading) {
            return this.renderLoading()
          }else if(this.props.allUsersQuery && this.props.allUsersQuery.error) {
            return this.renderError()
          }else{
          const userList = this.props.allUsersQuery.users
          return this.renderList(userList)
         }
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });

const ALL_USERS_QUERY = gql`query AllUsersQuery {
    users {
        _id
        firstName
        lastName
        email
        phone
        }
  }`;

export default graphql(ALL_USERS_QUERY, { name: 'allUsersQuery' }) (UserList);
