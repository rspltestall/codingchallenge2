import React, {Component} from 'react'
import {View,Text} from 'react-native'

export default class UserRow extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <View style={{flex:1,borderBottomWidth:0.5,borderBottomColor:'#cecece',marginLeft:5,marginRight:5,marginTop:5}}>
                <Text style={{fontSize:22,marginBottom:5,fontWeight:'bold'}}>
                    {this.props.data.firstName+' '+this.props.data.lastName}
                </Text>
                <Text style={{fontSize:16,marginBottom:3,fontWeight:'normal'}}>
                    Email : {this.props.data.email}
                </Text>
                <Text style={{fontSize:16,marginBottom:5,fontWeight:'normal'}}>
                    Phone : {this.props.data.phone}
                </Text>
            </View>
        )
    }
}